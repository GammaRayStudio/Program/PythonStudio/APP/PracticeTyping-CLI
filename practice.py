import os
from configparser import ConfigParser
from datetime import datetime


def loadConfig():
    config_object = ConfigParser()
    config_object.read("config.ini")
    practice_info = config_object["PRACTICE_INFO"]
    return practice_info


def loadKeywords():
    with open("keywords.txt", 'r') as f:
        lstKeywords = f.readlines()
    return lstKeywords


def choiceKeywords(lstKeywords):
    keyword = "abcdefghijklmnopqrstuvwxyz"

    description = ""
    for i in range(len(lstKeywords)):
        lstKeywords[i] = lstKeywords[i].strip('\n')
        description += str(i+1) + " : " + lstKeywords[i] + "\n"
    index = input("請決定練習的題目:\n" + description)
    if (int(index) - 1) < len(lstKeywords):
        keyword = lstKeywords[int(index)-1]

    return keyword


# 結束 : 匯出報告
def diffIndex(expect, actual):
    lstDiffIndex = []
    for i in range(len(expect)):
        if expect[i] != actual[i]:
            lstDiffIndex.append(i)
    return lstDiffIndex


def diffMsg(expect, lstDiffIndex):
    text = "diff  : "
    lstText = []
    for i in range(len(expect)):
        lstText.append(" ")

    for idx in lstDiffIndex:
        lstText[idx] = "^"
    for s in lstText:
        text += s
    text += " -> "
    for i in range(len(lstDiffIndex)):
        text += "'" + expect[lstDiffIndex[i]] + "'"
        if i < len(lstDiffIndex)-1:
            text += ","
    return text


def report(expect, count, correctCount, practiceTime, startTime, endTime):
    expectTime = practiceTime
    actualTiem = count
    correctTime = correctCount
    errorTime = count - correctCount
    correctRate = (correctCount / count) * 100
    totalTime = (endTime - startTime).total_seconds()
    speedRate = totalTime / count
    reportText = """
{0}
------
+ 期望次數 : {1}
+ 練習次數 : {2}
+ 正確次數 : {3}
+ 錯誤次數 : {4}
+ 正確率 : {5:.2f} %
+ 花費時間 : {6:.2f}
+ 平均速度 : {7:.2f}
+ 開始時間 : {8}
+ 結束時間 : {9}
    """.format(
        expect,
        expectTime,
        actualTiem,
        correctTime,
        errorTime,
        correctRate,
        totalTime,
        speedRate,
        startTime.strftime("%Y-%m-%d , %H:%M:%S"),
        endTime.strftime("%Y-%m-%d , %H:%M:%S")
    )
    print(reportText)
    return reportText


def export(reportText, exportPath):
    fileName = datetime.now().strftime("report_%Y%m%d_%H%M%S")
    if len(exportPath) > 0:
        pathText = exportPath + os.path.sep + fileName + '.md'
    else:
        pathText = fileName + '.md'
    with open(pathText, 'w') as f:
        f.write(reportText)
    print("練習報告輸出路徑 :", pathText)


def practice_process():
    configInfo = loadConfig()
    practiceTime = int(configInfo["practice_time"])  # 100
    exportPath = configInfo["export_path"]

    lstKeywords = loadKeywords()
    expect = choiceKeywords(lstKeywords)
    print("請輸入:", expect)

    isRun = True
    count = 0
    correctCount = 0

    startTime = datetime.now()
    endTime = datetime.now()
    totalTime = 0.0

    while(isRun):
        actual = input("練習第 {} 次 \n".format((count+1)))
        if expect == actual:
            print("input :", expect, "->", "正確 \n")
            correctCount += 1
        else:
            if len(expect) == len(actual):
                lstDiffIndex = diffIndex(expect, actual)
                text = diffMsg(expect, lstDiffIndex)
                print("input :", actual, "->", "錯誤")
                print(text, "\n")
            else:
                print("input :", actual, "->", "錯誤")
                print("diff  :", expect, "->", "長度無法匹配\n")

        count += 1
        if count >= practiceTime:
            isRun = False
            # 輸出練習的結果報告
        elif actual == "exit":
            isRun = False

        if isRun == False:
            endTime = datetime.now()
            reportText = report(expect, count, correctCount,
                                practiceTime, startTime, endTime)
            export(reportText, exportPath)


if __name__ == '__main__':
    practice_process()
